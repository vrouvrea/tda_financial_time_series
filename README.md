# TDA of financial time series

This web app is a reimplementation of the paper
[TDA of financial time series: Landscapes of crashes](https://arxiv.org/abs/1703.04385)
using [gudhi](https://gudhi.inria.fr).


## How to run it?

### On a binder instance

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fvrouvrea%2Ftda_financial_time_series/master?urlpath=proxy/8501/)

Binder instance creation may take some time, but it requires no local installation.

### Local installation

One can install it easily from conda:

* Install [miniconda3](https://conda.io/miniconda.html) environment .
* Create a new conda environment and activate it:

```bash
conda env create -f .binder/environment.yml
conda activate streamlit_financial_tda
```

### Run the web app

```bash
streamlit run tda_web_app.py
```

## Data generation

Data are generated using Yahoo! finance pip module to get the four major US stock market
indices: S&P 500, DJIA, NASDAQ, and Russel 2000. To install Yahoo! finance pip module:

```bash
conda install pip --yes
pip install yfinance
```

To generate the data:

```bash
python data_generation.py
```

A new version of `latest.csv` is generated.