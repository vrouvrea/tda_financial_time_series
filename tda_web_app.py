# Some basic data science imports
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline

# Some gudhi imports for TDA
from tda_pipeline import DataSelector, RipsPersistence, LPNorm
from gudhi.representations import Landscape

# Some graphical imports for the web app
import streamlit as st
import datetime
import plotly.express as px

# Use 100% of the page
def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>    
    """,
        unsafe_allow_html=True,
    )

_max_width_()

st.title('TDA of financial time series')
st.markdown('This web app is a reimplementation of the paper '
            '[TDA of financial time series: Landscapes of crashes](https://arxiv.org/abs/1703.04385) '
            'using [gudhi](https://gudhi.inria.fr). '
            'Select a start and an end date (default one is to visualize the dotcom crash).')
st.markdown('You can visualize **dotcom** crash : start=1999/03/10, end = 2000/03/10 | '
            '**Lehman brothers** crash : start=2007/09/15, end = 2008/09/15.')
st.markdown('In the publication, they use a 250 days (a little bit more than 1 year). '
            'You can set smaller date range if you find the computation not fast enough.')

# cf. data_generation.py
df = pd.read_csv('latest.csv', index_col=0)

# For widgets to be side by side
col1, col2, col3 = st.columns(3)
# min and max values taken from the dataset - Python 3.7+
min_value = datetime.date.fromisoformat(df.index[0])
max_value = datetime.date.fromisoformat(df.index[-1])

start = col1.date_input("start", datetime.date(1999, 3, 10), min_value=min_value, max_value=max_value)
end = col2.date_input("end", datetime.date(2000, 3, 10), min_value=min_value, max_value=max_value)
w = col3.text_input("Windows size", 50)

def nearest_str_date(items, pivot):
    return min(items, key=lambda str_date: abs(datetime.date.fromisoformat(str_date) - pivot))

if st.button('compute'):
    w=int(w)
    start=nearest_str_date(df.index, start - datetime.timedelta(days=w))
    end=nearest_str_date(df.index, end)
    start_idx = df.index.get_loc(str(start))
    end_idx = df.index.get_loc(str(end))
    # Some error management
    if start_idx == end_idx:
        raise IndexError('Start date must be different from end date.')
    if end_idx < w:
        end_idx = w
    if end_idx < start_idx:
        start_idx, end_idx = end_idx, start_idx

    pipe = Pipeline(
        [
            ("data_sel", DataSelector(start=start_idx, end=end_idx, w=w)),
            ("rips_pers", RipsPersistence(max_rips_dimension=2, max_persistence_dimension=2, only_this_dim=1, n_jobs=-1)),
            ("landscape", Landscape(resolution=1000)),
            ("lpnorm", LPNorm(n_jobs=-1)),
            ("mms", MinMaxScaler()),
        ]
    )

    L1L2mms = pipe.fit_transform(df)
    l1l2df = pd.DataFrame({'date': df[start_idx+w:end_idx].index, 'L1': L1L2mms.transpose()[0], 'L2': L1L2mms.transpose()[1]})
    # Calculate the variance for the L norms
    j = 0

    for i in l1l2df.index:
        if j < w:
            k = 0
        else:
            k = j - w
        l1l2df.loc[i, 'L1_variance'] = np.var(l1l2df.iloc[k:j].L1)
        l1l2df.loc[i, 'L2_variance'] = np.var(l1l2df.iloc[k:j].L2)
        j += 1
    
    fig = px.line(l1l2df, x="date", y=l1l2df.columns,
                  hover_data={"date": "|%B %d, %Y"},
                  title='Lp Norm')
    fig.update_xaxes(
        dtick="M1",
        tickformat="%b\n%Y")
    st.plotly_chart(fig, use_container_width=True)
